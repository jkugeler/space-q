export default class TitleScene extends Phaser.Scene {
    constructor() {
        super('TitleScene');
    }

    preload() {
        this.load.image('space_background', './assets/space_background.webp')
        this.load.image('startButton', './assets/start-button.png');
        this.load.image('buttonHover', './assets/start-button-hover.png')
    }
    

    create() {
        // Add Background
        this.background = this.add.image(0, 0, 'space_background').setOrigin(0, 0);

        //Add Space-Q-Text
        this.add.text(400, 200, 'Space-Q', { font: '132px Arial Black', fill: '#195564' }).setOrigin(0.5);

        // Add Start-Button
        let startButton = this.add.image(400, 400, 'startButton').setInteractive();
        startButton.setOrigin(0.5, 0.5);

        // Button hover effect
        startButton.on('pointerover', () => {
            startButton.setTexture('buttonHover');
        });
    
        startButton.on('pointerout', () => {
            startButton.setTexture('startButton');
        });

        startButton.on('pointerdown', () => {
            
            this.scene.start('GameScene');
            }
        );
    }
}