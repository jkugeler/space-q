import Phaser from 'phaser'

export default class GameScene extends Phaser.Scene {
    constructor(key) {
        super(key)
        this.eventEmitter = new Phaser.Events.EventEmitter();
        this.xcount = 0;
        this.hcount = 0;
        this.zcount = 0;
    }

    init() {
        this.data.set('eventEmitter', new Phaser.Events.EventEmitter());
    }
    
    preload() {
        this.load.image('space_background', 'assets/space_background.webp');
        this.load.image('rocket', 'assets/rocket.png');
        this.load.image('h-gate', 'assets/h-gate.png');
        this.load.image('x-gate', 'assets/x-gate.png');
        this.load.image('z-gate', 'assets/z-gate.png');

        this.load.audio('bg_music', 'assets/bg_music.mp3');
        this.load.audio('collect_gate', 'assets/collect_gate.mp3');
    }

    create() {
        
        // Add two background images, positioned side by side
        this.background1 = this.add.image(0, 0, 'space_background').setOrigin(0, 0);
        this.background2 = this.add.image(this.background1.height, 0, 'space_background').setOrigin(0, 0);

        // Manually call the resize method with the current game size
        this.resize({ width: this.scale.width, height: this.scale.height });

        // Set up resize event listener
        this.scale.on('resize', this.resize, this);

        //Add Rocket Image as character
        this.rocket = this.physics.add.image(400, 300, 'rocket')
        this.rocket.setOrigin(0.5, 0.5);
        this.rocket.setScale(0.1);

        // Create cursor keys for rocket control
        this.cursors = this.input.keyboard.createCursorKeys();

        // Add music
        this.cgMusic = this.sound.add('collect_gate');
        this.cgMusic.loop = false;

        this.bgMusic = this.sound.add('bg_music');
        this.bgMusic.loop = true;
        this.bgMusic.play();
    }

    resize(gameSize) {
        const width = gameSize.width;
        const height = gameSize.height;
    
        this.cameras.main.setViewport(0, 0, width, height);
    
        // Resize and reposition background images
        if (this.background1 && this.background2) {
            const scale = height / this.background1.height;
            this.background1.setScale(scale);
            this.background2.setScale(scale);
            this.background1.setPosition(0, 0);
            this.background2.setPosition(this.background1.displayWidth, 0);
        }

        // Adjust rocket image
        if (this.rocket) {
            // Original rocket dimensions
            const originalRocketWidth = 1013; 
            const originalRocketHeight = 557; 

            // Calculate the scale factor
            const rocketScaleFactor = Math.min(
                Math.min(width / originalRocketWidth, height / originalRocketHeight),
                0.1 // The maximum scale factor
            );

            // Apply the scale factor to the rocket
            this.rocket.setScale(rocketScaleFactor);
        }
    }

    update(time, delta) {
        const backgroundSpeed = 2; // Speed of background scrolling
        const rocketSpeed = 5; // Speed of the rocket

        // Background Scrolling
        this.background1.x -= backgroundSpeed;
        this.background2.x -= backgroundSpeed;

        // Reset background position
        if (this.background1.x + this.background1.displayWidth < 0) {
            this.background1.x = this.background2.x + this.background2.displayWidth;
        }
        if (this.background2.x + this.background2.displayWidth < 0) {
            this.background2.x = this.background1.x + this.background1.displayWidth;
        }

        // rocket Movement with Boundary Checks
        if (this.cursors.left.isDown && this.rocket.x > 0) {
            this.rocket.x -= rocketSpeed;
        } else if (this.cursors.right.isDown && this.rocket.x < this.scale.width - this.rocket.displayWidth / 2) {
            this.rocket.x += rocketSpeed;
        }

        if (this.cursors.up.isDown && this.rocket.y > 0) {
            this.rocket.y -= rocketSpeed;
        } else if (this.cursors.down.isDown && this.rocket.y < this.scale.height - this.rocket.displayHeight) {
            this.rocket.y += rocketSpeed;
        }
    }

    collectGate(gate, gateType) {
        switch (gateType) {
            case 'x':
                this.cgMusic.play();
                this.xcount += 1;
                console.log("X-gate collected. Total collected:", this.xcount);
                this.xGateCounterText.setText('X: ' + this.xcount);
                break;
            case 'h':
                this.hcount += 1;
                console.log("H-gate collected. Total collected:", this.hcount);
                this.hGateCounterText.setText('H: ' + this.hcount);
                break;
            case 'z':
                this.zcount += 1;
                console.log("Z-gate collected. Total collected:", this.zcount);
                this.zGateCounterText.setText('Z: ' + this.zcount);
                break;
            default:
                console.log("Unknown gate type collected");
        }
    
        if (gateType === 'x' && !this.hasShownGateCollectedPopUp) {
            // Show the gate-collected pop-up only for the first X-Gate collected
            this.showGateCollectedPopUp();
            this.hasShownGateCollectedPopUp = true;
        }
    
        // Show End-Level-Pop-up when second x-gate in level 1 is collected
        if (gateType === 'x' && this.xcount === 2 && !this.levelEnded) {
            console.log(`Gate collected: ${gateType}, xcount: ${this.xcount}, levelEnded: ${this.levelEnded}`);
            if (this.eventEmitter) {
                this.eventEmitter.emit('level1End');
                console.log("level1End event emitted");
            } else {
                console.log("eventEmitter is undefined!");
            }
            this.levelEnded = true;
        } else {
            console.log(`Gate collected, but not second X-gate or level already ended. xcount: ${this.xcount}, levelEnded: ${this.levelEnded}`);
        }

        if (this.scene.key === 'GameSceneLevel1') { 
            if (gateType === 'x' && this.xcount === 1) { 
                this.cloudText.setText('|0>'); // Update the cloudText
            }
        }

        // Emitting a general 'gateCollected' event
        if (this.eventEmitter) {
            this.eventEmitter.emit('gateCollected', gateType);
        } else {
            console.log("eventEmitter is undefined for gateCollected event!");
        }

    
        // Reposition the gate
        gate.setPosition(this.scale.width + gate.width, Phaser.Math.Between(100, this.scale.height - 100));
    }
}