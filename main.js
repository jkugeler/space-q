import './style.css';
import Phaser from 'phaser'
import TitleScene from './scenes/TitleScene'
import LevelSelectionScene from './scenes/LevelSelectionScene';
import GameSceneLevel1 from './scenes/GameSceneLevel1';

const config = {
    type: Phaser.AUTO,
    parent: 'game-container',
    width: 800,
    height: 600,
    scale: {
        mode: Phaser.Scale.FIT,
        autoCenter: Phaser.Scale.CENTER_BOTH // Center the game canvas in the browser
    },
    physics: {
        default: 'arcade',
        arcade: {
            gravity: { y: 0 },
            // debug: true
        }
    },
    scene: [
        TitleScene, LevelSelectionScene, GameSceneLevel1
]};

new Phaser.Game(config);

